pValue=MsgBox("Select Yes of Automatic, No for Manual and Cancel for off", 3, "Set Proxy")
If pValue <> "" Then
  select case pValue
    case vbYes
      ManualProxy  "off" 
      IEautomaticallydetect  "on"
      MsgBox "Automatic proxy is set"
    case vbNo
      ManualProxy  "on" 
      IEautomaticallydetect  "man"
      MsgBox "Manual proxy is set"
    case vbCancel
      ManualProxy  "off"
      IEautomaticallydetect  "off"
      MsgBox "Proxy setting is cleared"
  end select
Else
  MsgBox "You did not click a button"
End If
 
 SUB ManualProxy (status)
 Set WSHShell = CreateObject("Wscript.Shell")
 select case lcase(status)
   case "on"
     WSHShell.RegWrite "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ProxyServer", "proxy.logica.com:80", "REG_SZ"
     'WSHShell.RegWrite "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ProxyServer", "158.234.204.23:80", "REG_SZ"
     WSHShell.RegWrite "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ProxyEnable", 1, "REG_DWORD"
   case "off" 
     WSHShell.RegWrite "HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings\ProxyEnable", 0, "REG_DWORD"
   case else
     wscript.echo "Invalid parameter - IEautomaticallydetect  on, off or show"
 end select
 end sub
 
 SUB IEautomaticallydetect (status) 
  
 'howeasyisthat.com 
 'The parameter shold be set to 'off', 'on' or 'show'. 
 'This reads and writes to  HKCU\Software\Microsoft\Windows\CurrentVersion\Internet Settings\Connections\DefaultConnectionSettings in order to 
 'set or read the Internet Explorer Automatically Detect facility. When a proxy is set, this should normally be off. When no proxy settings are set, 
 'it's usually best to have this setting turned on.[/size] 
  
 DIM sKey,sValue,binaryVal 
 Dim oReg 
 Set oReg=GetObject( "winmgmts:{impersonationLevel=impersonate}!\\.\root\default:StdRegProv")    'For registry operations througout 
  
 Const HKCU=&H80000001 
  
 sKey = "Software\Microsoft\Windows\CurrentVersion\Internet Settings\Connections" 
 sValue = "DefaultConnectionSettings" 
  
 oReg.GetBinaryValue HKCU, sKey, sValue, binaryVal 
  
 select case lcase(status) 
   case "on"
     binaryVal(8) = binaryVal(8) OR 2 XOR 2        'Manual off
     binaryVal(8) = binaryVal(8) OR 8              'Autodetect on
   case "off"    
     binaryVal(8) = binaryVal(8) OR 8 XOR 8        'Force Autodetect off 
     binaryVal(8) = binaryVal(8) OR 2 XOR 2        'Manual off
   case "man"
     binaryVal(8) = binaryVal(8) OR 8 XOR 8        'Force Autodetect off 
     binaryVal(8) = binaryVal(8) OR 2              'Manual On' 
   case "show"    wscript.echo "Automatically detect is set to " & ((binaryVal(8) and 8) = 8) 
   case else    wscript.echo "Invalid parameter - IEautomaticallydetect  on, off or show" 
 end select 
  
 if lcase(status)="on" or lcase(status)="off" or lcase(status)="man" then oReg.SetBinaryValue HKCU, sKey, sValue, binaryVal 
  
 end sub
# README #


### What is this repository for? ###

This program can be used for quickly changing the proxy configurations. It has three options: 

* **yes**: Set it to automatic
* **no**: Set it to manual - You will have to modify the code to put the manual proxy you want
* **cancel**: Set the proxy to direct connection

### How do I get set up? ###

Just download proxy_changer.vbs. Modify the URL for manual proxy and run it.